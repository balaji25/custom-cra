import React, { Component } from "react";
import "./App.css";

function App() {
  const [state, setState] = React.useState([]);
  React.useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((json) => setState(json));
  }, []);
  return (
    <div className="App">
      <h1>Hubilo Feeds</h1>
      <hr style={{ backgroundColor: "blue" }} />
      {state &&
        state.map((item) => {
          return (
            <div key={item.id}>
              <h2>{item.title}</h2>
              <p>{item.body}</p>
              <hr />
            </div>
          );
        })}
    </div>
  );
}

export default App;
